
DESCRIPTION
-----------
This is a module which provides an interface for managing value lists in 
fringe use cases where CCK is not being used to define select element
options. For example, if you have a situation where you have a
programatically-defined form with select elements that an end user needs
to be able to adjust over time, this module allows you to define those
lists via Drupal, then grant access to edit those lists to other users.

INSTALLATION
------------
Place the entire 'valuelist' directory into your site's 'modules' 
directory (e.g. sites/all/modules).  While logged into your Drupal site
as an administrator, go to the http://mysite.com/admin/build/modules page,
check the box next to 'Value List', and click 'Save Configuration'.  
Module permissions may then be configured by going to 
http://mysite.com/admin/user/access#module-valuelist.

MANAGING VALUE LISTS
--------------------
Value lists can be managed at http://mysite.com/admin/build/valuelist.

Enter one value per line, in the format 'value|label', where 'value' is
the actual value that will be submitted with the form, and 'label' is
the string that will be displayed in the select pull-down.

If value and label are the same, just enter a single string.

USING VALUE LISTS IN FORMS
--------------------------
Once you have defined a value list, you can retrieve that list as an
associative array suitable for use in a Form API 'select' element using 
the valuelist_get_array() function.  For example, take a value list called
"instruments" with the following values:

bjo|Banjo
gtr|Guitar
mdo|Mandolin

A 'select' element using this value list would look something like this:

<?php
$form['instruments'] = array(
  '#type' => 'select',
  '#title' => t('Instruments'),
  '#options' => valuelist_get_array('instruments')
);
?>

'#options' will now contain an associative array: 

array('bjo' => 'Banjo', 'gtr' => 'Guitar', 'mdo' => 'Mandolin)

Label values are run through the t() function.

CREDITS
-------
Developed and maintained by Andy Chase <http://proofgroup.com>
